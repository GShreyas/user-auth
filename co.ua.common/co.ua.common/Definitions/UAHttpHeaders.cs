﻿namespace co.ua.common.Definitions
{
    public class UAHttpHeaders
    {
        public static readonly string AccountName = "AccountName";
        public static readonly string ClientID = "ClientID";
        public static readonly string Authorization = "Authorization";
        public static readonly string TokenID = "TokenID";
        public static readonly string UserGUID = "UserGUID";

        public static readonly string Host = "Host";
        public static readonly string UserAgent = "User-Agent";
        public static readonly string Referer = "Referer";
    }
}
