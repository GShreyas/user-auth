﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace co.ua.common
{
    public class Constants
    {
        public static readonly int maxUnsuccessfullLoginAttempts = 5;
        public static readonly string privateKeyName = "private.key.pem";
        public static readonly string privateKeyPath = "RSA";

        public static readonly string ua_SP_CheckUser = "spCheckUser {0}, {1}";
        public static readonly string ua_SP_GetUserDetails = "spGetUserDetails {0}, {1}";
        public static readonly string ua_SP_CreateTokenInDB = "spCreateTokenInDB {0}, {1}, {2}, {3}, {4}, {5}";

        public static readonly string ua_SP_UpsertAccessMatrix = "spUpsertUserAccessMatrix {0}, {1}, {2}, {3}, {4}, {5}";
        public static readonly string ua_SP_DeleteAccessMatrix = "spDeleteUserAccessMatrix {0}, {1}";
        public static readonly string ua_SP_GetAccessMatrix = "spGetUserAccessMatrix";

        public static readonly string ua_SP_UpsertAttribute = "spUpsertAttributes {0}, {1}, {2}, {3}, {4}";
        public static readonly string ua_SP_DeleteAttribute = "spDeleteAttribute {0}, {1}";
        public static readonly string ua_SP_GetAttributes = "spGetAttributes";

        public static readonly string ua_SP_UpsertRole = "spUpsertRole {0}, {1}, {2}, {3}, {4}";
        public static readonly string ua_SP_GetAllRoles = "spGetAllRoles {0}";
        public static readonly string ua_SP_DeleteRole = "spDeleteRole {0}, {1}";
        public static readonly string ua_SP_GetRoleByUserName = "spGetRolesFortheUser {0}";

        public static readonly string ua_SP_UpsertUserAttribute = "spUpsertUserAttribute {0}, {1}, {2}, {3}, {4}, {5}";
        public static readonly string ua_SP_DeleteUserAttribute = "spDeleteUserAttribute {0}, {1}";
        public static readonly string ua_SP_GetUserAttributes = "spGetUserAttrributes";

        public static readonly string ua_SP_DeleteUser = "spDeleteUser {0}, {1}";
        public static readonly string ua_SP_GetAllUsers = "spGetAllUsers {0}";
        public static readonly string ua_SP_UpsertUser = "spUpsertUser {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}";

        public static readonly string ua_SP_LogOff = "spLogOff {0}, {1}, {2}";

        public static readonly string ua_SP_GetTokenByTokenID = "spGetTokenByTokenID {0}";

        public static readonly string ua_SP_GetUserNameByUserGuid = "spGetUserNameByUserGuid {0}";

        public static readonly string ua_SP_CheckUserAccess = "spCheckUserAccess {0}, {1}, {2}";

        public static readonly string ua_SP_UpdateExceptionlog = "spUpdateExceptionLog {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}";

        public static readonly string ua_SP_UpdateUserAuditLog = "spUpdateUserAuditLog {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}";

        public static readonly string ua_SP_UpdateUserLoginStatus = "spUpdateUserLoginStatus {0}, {1}";

        public static readonly string ua_sp_CheckUserAccess = "spCheckUserAccess {0}, {1}, {2}";





    }
}
