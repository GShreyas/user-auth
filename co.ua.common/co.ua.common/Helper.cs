﻿using Microsoft.Extensions.Configuration;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace co.ua.common
{
    static class ConfigurationManager
    {
        public static IConfiguration AppSetting { get; }

        static ConfigurationManager()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
        }
    }

    public static class Helper
    {

        public static string GetConnectionString
        {
            get
            {
                return ConfigurationManager.AppSetting["ConnectionStrings:UADatabase"].ToString();
            }
        }

        public static string GetLoggingConnectionString
        {
            get
            {
                return ConfigurationManager.AppSetting["ConnectionStrings:UALoggingDatabase"].ToString();
            }
        }
        public static string GetDecryptPassword
        {
            get
            {
                return ConfigurationManager.AppSetting["AppSetting:DecryptPassword"].ToString();
            }
        }

        public static string DecryptValue(string cipherText)
        {
            string password = GetDecryptPassword;

            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using Aes encryptor = Aes.Create();
            // extract salt (first 16 bytes)
            var salt = cipherBytes.Take(16).ToArray();
            // extract iv (next 16 bytes)
            var iv = cipherBytes.Skip(16).Take(16).ToArray();
            // the rest is encrypted data
            var encrypted = cipherBytes.Skip(32).ToArray();
            Rfc2898DeriveBytes pdb = new(password, salt, 100);
            encryptor.Key = pdb.GetBytes(32);
            encryptor.Padding = PaddingMode.PKCS7;
            encryptor.Mode = CipherMode.CBC;
            encryptor.IV = iv;
            
            using MemoryStream ms = new(encrypted);
            using CryptoStream cs = new(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Read);
            using var reader = new StreamReader(cs, Encoding.UTF8);
            return reader.ReadToEnd();
        }
        public static string GetClientIP
        {
            get
            {
                string Str = Dns.GetHostName();
                IPHostEntry ipEntry = Dns.GetHostEntry(Str);
                IPAddress[] addr = ipEntry.AddressList;

                if (addr != null && addr.Length > 0)
                    return addr[^1].ToString();
                else
                    return "";
            }
        }

    }
}