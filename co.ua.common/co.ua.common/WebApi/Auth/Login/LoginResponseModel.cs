﻿using co.ua.common.WebApi.Auth.User;

namespace co.ua.common.WebApi.Auth.Login
{
    public class LoginResponseModel
    {
        public bool IsAuthSuccessful { get; set; }
        public string ErrorMessage { get; set; }
        public UserDetailsModel User { get; set; }
    }
}
