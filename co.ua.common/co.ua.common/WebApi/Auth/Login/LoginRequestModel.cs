﻿
namespace co.ua.common.WebApi.Auth.Login
{
    public class LoginRequestModel
    {
        public string AesKey { get; set; }
        public string Data { get; set; }
    }
}
