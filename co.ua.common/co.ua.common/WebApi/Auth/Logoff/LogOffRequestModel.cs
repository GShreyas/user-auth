﻿
using co.ua.common.WebApi.Request;

namespace co.ua.common.WebApi
{
    public class LogOffRequestModel: RequestModel
    {
        public string UserName { get; set; }
        
    }
}
