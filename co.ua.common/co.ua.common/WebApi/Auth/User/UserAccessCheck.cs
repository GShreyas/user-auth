﻿using co.ua.common.WebApi.Response;

namespace co.ua.common.WebApi.Auth.User
{
    public class UserAccessCheckRequestModel
    {
        public Guid? UserGUID { get; set; }
        public string? PathURL { get; set; }
        public string? AccessLevel { get; set; }
    }

    public class UserAccessCheckResponseModel: ResponseInheritanceModel
    {
        public int? TotalAccessCount { get; set; }
        public string AccessLevels { get; set; }
    }
}
