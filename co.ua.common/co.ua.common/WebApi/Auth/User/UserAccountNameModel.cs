﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace co.ua.common.WebApi.Auth.User
{
    public class UserAccountNameModel
    {
        [Key]
        public Guid UserID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { set; get; }
    }

}
