﻿
using co.ua.common.WebApi.Response;

namespace co.ua.common.WebApi.Auth.User
{
    public class UserCheckResponseModel: ResponseInheritanceModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string? Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsValidUser { get; set; }
        public bool IsUserLocked { get; set; }
    }
}
