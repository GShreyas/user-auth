﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace co.ua.common.WebApi.Auth.User
{
    public class AESUser
    {
        public string Username { get; set; }
        public string ClientIP { get; set; }
        public string Password { get; set; }
    }
}
