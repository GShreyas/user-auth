﻿
namespace co.ua.common.WebApi.Request
{
    public class RequestModel
    {
        public Guid? UserGUID { get; set; }
        public string? ClientIP { get; set; }

    }
}
