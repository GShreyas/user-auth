﻿
using co.ua.common.WebApi.Request;

namespace co.ua.common.WebApi.Token
{
    public class RefreshTokenRequestModel : RequestModel
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
