﻿using co.ua.common.WebApi.Request;
using System.ComponentModel.DataAnnotations;

namespace co.ua.common.WebApi.Role
{
    public class Role
    {
        [Key]
        public Guid? RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public bool IsActive { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? ModifiedById { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class RoleRequestModel : RequestModel
    {
        public Guid? RoleId { get; set; }
        public string? RoleName { get; set; }
        public string? RoleDescription { get; set; }
        public Guid? CopiedRoleId { get; set; }
    }

}

