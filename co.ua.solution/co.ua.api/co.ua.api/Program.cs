using co.ua.api.Helpers;
using co.ua.api.Models;
using co.ua.api.Utilities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using mss.api.Middleware;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Configuration
builder.Configuration.AddJsonFile("appsettings.json");
var jwtSettings = builder.Configuration.GetSection("JwtSettings");

builder.Services.AddControllers();
builder.Services.AddDbContext<UAContext>();


builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy", builder => builder
        .AllowAnyMethod()   // .WithMethods("GET", "POST")
        .AllowAnyHeader()   // .WithHeaders("Content-Type", "Authorization")
        .AllowAnyOrigin() // .WithOrigins("https://example.com", "https://anotherdomain.com")
                          //  .SetIsOriginAllowed(origin => true) // allow any origin;
    );
});

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateLifetime = true,
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = jwtSettings.GetSection("validIssuer").Value,
        ValidAudience = jwtSettings.GetSection("validAudience").Value,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.GetSection("securityKey").Value)),
        // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
        ClockSkew = TimeSpan.Zero
    };
    options.Events = new JwtBearerEvents
    {
        OnAuthenticationFailed = context =>
        {
            if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
            {
                context.Response.Headers.Add("Token-Expired", "true");
            }
            if (context.Exception.GetType() == typeof(SecurityTokenValidationException))
            {
                context.Response.Headers.Add("Token-Invalid", "true");
            }
            return Task.CompletedTask;
        }
    };
});

builder.Services.AddScoped<JWTUtility>();
builder.Services.AddTransient<IAESHelper, AESHelper>();
builder.Services.AddTransient<IRSAHelper, RSAHelper>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseMiddleware<ExceptionHandlingMiddleware>();
app.UseStatusCodePages();
app.UseRouting();
app.UseCors("CorsPolicy");
app.UseHttpsRedirection();
//app.UseAuthentication();
//app.UseAuthorization();

app.MapControllers();

app.Run();