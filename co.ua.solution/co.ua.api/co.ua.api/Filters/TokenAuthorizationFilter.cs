﻿using co.ua.api.Models;
using co.ua.api.Services;
using co.ua.api.Utilities;
using co.ua.common;
using co.ua.common.Definitions;
using co.ua.common.WebApi.Auth.Login;
using co.ua.common.WebApi.Token;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

namespace co.app.api.Filters
{
    public class TokenAuthorizationFilter: ActionFilterAttribute
    {

        private readonly UAContext _context;
        private readonly TokenValidatorService TokenValidator;
        private readonly JWTUtility _jwtUtility;

        public TokenAuthorizationFilter(
            UAContext context, JWTUtility jwtUtil
            )
        {
            _context = context;
            _jwtUtility = jwtUtil;

        }

        public override void OnActionExecuting(ActionExecutingContext actionExecutingContext)
        {
            try
            {
                int TokenID = int.Parse(actionExecutingContext.HttpContext.Request.Headers[UAHttpHeaders.TokenID]);

                var arrayOfTokens = _context.TokenModel.FromSqlRaw(
                    Constants.ua_SP_GetTokenByTokenID, TokenID).ToList();

                if (arrayOfTokens.Count == 0 || arrayOfTokens.Count > 1)
                {
                    actionExecutingContext.Result = new ObjectResult(actionExecutingContext.ModelState)
                    {
                        Value = null,
                        StatusCode = StatusCodes.Status403Forbidden
                    };
                }

                TokenModel targetToken = arrayOfTokens[0];

                /* if (targetToken.TokenValidTo > DateTime.Now ||
                       !TokenValidator.DoValidation(arrayOfTokens[0], actionExecutingContext)
                       )
                   {
                       actionExecutingContext.Result = new ObjectResult(actionExecutingContext.ModelState)
                       {
                           Value = null,
                           StatusCode = StatusCodes.Status403Forbidden
                       };
                   } */

                if (TokenValidator.DoValidation(targetToken, actionExecutingContext))
                {
                    if (targetToken.TokenValidTo > DateTime.Now)
                    {
                        var username = targetToken.GrantedTo;
                        var clientIp = Helper.GetClientIP;

                        var userDetails = _context.UserDetailsModel.FromSqlRaw(Constants.ua_SP_GetUserDetails, username, clientIp).ToList()[0];

                        TokenModel jwtToken = _jwtUtility.GetTokenObject(actionExecutingContext.HttpContext, userDetails); ;


                        TokenModel tokenResult = _context.TokenModel.FromSqlRaw(
                           Constants.ua_SP_CreateTokenInDB, userDetails.UserName, jwtToken.CurrentToken, jwtToken.CurrentToken,
                           jwtToken.TokenValidFrom.Value, jwtToken.TokenValidTo.Value, jwtToken.ClientID).ToList()[0];

                        userDetails.TokenID = tokenResult.ID;
                        userDetails.Token = jwtToken.CurrentToken;
                        userDetails.RefreshToken = jwtToken.RotativeToken;
                        userDetails.IsError = false;
                        userDetails.ErrorMessage = "";
                        userDetails.ValidateResponse = "";


                        //return new LoginResponseModel { IsAuthSuccessful = true, User = userDetails, ErrorMessage = "" };

                        actionExecutingContext.Result = new ObjectResult(new
                        {
                            userDetails
                        });

                    }

                }
                else
                {
                    actionExecutingContext.Result = new ObjectResult(actionExecutingContext.ModelState)
                    {
                        Value = null,
                        StatusCode = StatusCodes.Status403Forbidden
                    };
                }

            }
            catch(Exception ex)
            {
                actionExecutingContext.Result = new ObjectResult(actionExecutingContext.ModelState)
                {
                    Value = null,
                    StatusCode = StatusCodes.Status403Forbidden
                };

            }

          
        }
    }
}
