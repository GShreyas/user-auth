﻿namespace co.ua.api.Models
{
    public interface IAESHelper
    {
        string Decrypt(string value, string aesKey);
    }
}
