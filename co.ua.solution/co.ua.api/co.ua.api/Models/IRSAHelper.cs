﻿namespace co.ua.api.Models
{
    public interface IRSAHelper
    {
        string Decrypt(string value);
    }
}
