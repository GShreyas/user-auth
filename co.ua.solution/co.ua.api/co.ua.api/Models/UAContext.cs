﻿using co.ua.common;
using co.ua.common.WebApi.Auth.User;
using co.ua.common.WebApi.Response;
using co.ua.common.WebApi.Role;
using co.ua.common.WebApi.Token;
using Microsoft.EntityFrameworkCore;

namespace co.ua.api.Models
{
    public class UAContext: DbContext
    {
        public UAContext(DbContextOptions<UAContext> options) : base(options)
        {
            Database.SetCommandTimeout(120000);
            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            optionBuilder.UseSqlServer(Helper.GetConnectionString);
        }

        public virtual DbSet<ResponseModel> GetResponseWithNoDataReturn { get; set; }
        public virtual DbSet<ResponseModelWithGuid> GetResponseWithGuid { get; set; }
        public virtual DbSet<UserCheckResponseModel> UserCheckResponseModel { get; set; }
        public virtual DbSet<UserDetailsModel> UserDetailsModel { get; set; }
        public virtual DbSet<TokenModel> TokenModel { get; set; }
       // public virtual DbSet<UserModel> Users { get; set; }
        public virtual DbSet<Role> UpsertRole { get; set; }
        //public virtual DbSet<AccessMatrixListModel> AccessMatrixListModels { get; set; }
        //public virtual DbSet<AttributeListModel> AttributeListModels { get; set; }
        public virtual DbSet<Role> RoleModels { get; set; }
        //public virtual DbSet<UserAttribute> UserAttributes { get; set; }
        public virtual DbSet<UserAccountNameModel> UserAccountNameModel { get; set; }
        public virtual DbSet<UserAccessCheckResponseModel> UserAccessCheckResponseModel { get; set; }



    }
}
