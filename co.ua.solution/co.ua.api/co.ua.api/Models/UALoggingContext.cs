﻿using Microsoft.EntityFrameworkCore;
using co.ua.common.WebApi.Response;

namespace co.ua.api.Models
{
    public class UALoggingContext: DbContext
    {
        public UALoggingContext()
        {

        }

        public UALoggingContext(DbContextOptions options)
           : base(options)
        {
            Database.SetCommandTimeout(120000);
            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        public virtual DbSet<ResponseModel> StandardResponseModel { get; set; }

    }
}
