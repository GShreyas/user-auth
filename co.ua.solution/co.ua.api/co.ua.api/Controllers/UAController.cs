﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace co.ua.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UAController : ControllerBase
    {
        public UAController()
        {
        }

        [Route("")]
        public string DisplayStartAPI()
        {
            return "UA API running!";
        }
    }
}
