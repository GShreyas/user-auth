﻿using co.ua.api.Models;
using co.ua.api.Utilities;
using co.ua.common.WebApi.Token;
using co.ua.common;
using Microsoft.AspNetCore.Mvc;
using co.ua.common.WebApi.Auth.Login;
using co.ua.common.WebApi.Auth.User;
using co.ua.common.WebApi.Response;
using System.Data;
using co.ua.common.WebApi;
using co.ua.common.WebApi.Role;
using Microsoft.EntityFrameworkCore;
using co.ua.api.Helpers;

namespace co.ua.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UAContext _context;
        private readonly JWTUtility _jwtUtility;
        private readonly IRSAHelper RSAHelper;
        private readonly IAESHelper AESHelper;
        public AuthController(UAContext context, JWTUtility jwtUtil, IRSAHelper rsaHelper, IAESHelper aesHelper)
        {
            _context = context;
            _jwtUtility = jwtUtil;
            RSAHelper = rsaHelper;
            AESHelper = aesHelper;
        }

        [HttpPost]
        [Route("validate-user")]
        public LoginResponseModel Login([FromBody] LoginRequestModel requestBody)
        {
            try
            {

                var keyValue = RSAHelper.Decrypt(requestBody.AesKey);
                var userJson = AESHelper.Decrypt(requestBody.Data, keyValue);
                AESUser? cleanUser = Newtonsoft.Json.JsonConvert.DeserializeObject<AESUser>(userJson);


                var username = cleanUser.Username;
                var password = cleanUser.Password;

                var clientIp = Helper.GetClientIP;

                var checkUserResult = _context.UserCheckResponseModel.FromSqlRaw(Constants.ua_SP_CheckUser, username, clientIp).ToList()[0];

                if (checkUserResult.IsValidUser)
                {

                    var passwordFromDb = Helper.DecryptValue(checkUserResult.Password);
                    if (passwordFromDb == password)
                    {
                        var userDetails = _context.UserDetailsModel.FromSqlRaw(Constants.ua_SP_GetUserDetails, username, clientIp).ToList()[0];

                        TokenModel jwtToken = GetToken(userDetails);

                        TokenModel tokenResult = _context.TokenModel.FromSqlRaw(
                           Constants.ua_SP_CreateTokenInDB, userDetails.UserName, jwtToken.CurrentToken, jwtToken.CurrentToken,
                           jwtToken.TokenValidFrom.Value, jwtToken.TokenValidTo.Value, jwtToken.ClientID).ToList()[0];

                        userDetails.TokenID = tokenResult.ID;
                        userDetails.Token = jwtToken.CurrentToken;
                        userDetails.RefreshToken = jwtToken.RotativeToken;
                        userDetails.IsError = false;
                        userDetails.ErrorMessage = "";
                        userDetails.ValidateResponse = "";


                        return new LoginResponseModel { IsAuthSuccessful = true, User = userDetails, ErrorMessage = "" };
                    }
                    else
                    {

                        var userResult = _context.GetResponseWithNoDataReturn
                            .FromSqlRaw(
                            Constants.ua_SP_UpdateUserLoginStatus, checkUserResult.UserId, Constants.maxUnsuccessfullLoginAttempts).ToList()[0];


                        return new LoginResponseModel
                        {
                            IsAuthSuccessful = false,
                            User = { },
                            ErrorMessage = userResult.ValidateResponse
                        };

                    }

                }
                else
                if (!checkUserResult.IsValidUser || !checkUserResult.IsActive || checkUserResult.IsUserLocked)
                {
                    return new LoginResponseModel { IsAuthSuccessful = false, User = { }, ErrorMessage = checkUserResult.ErrorMessage };
                }
                else
                {
                    return new LoginResponseModel { IsAuthSuccessful = false, User = { }, ErrorMessage = "No Error" };
                }



            }
            catch (Exception ex)
            {
                return new LoginResponseModel { IsAuthSuccessful = false, User = { }, ErrorMessage = ex.Message };
            }

            TokenModel GetToken(UserDetailsModel user)
            {

                TokenModel Token = _jwtUtility.GetTokenObject(HttpContext, user);
                return Token;
            }


        }

        [HttpPost]
        [Route("log-off")]
        public ResponseModel LogOff([FromBody] LogOffRequestModel userReqModel)
        {

            try
            {
                var userName = Helper.DecryptValue(userReqModel.UserName);

                var result = _context.GetResponseWithNoDataReturn.FromSqlRaw(Constants.ua_SP_LogOff, userReqModel.UserGUID, userName, Helper.GetClientIP).ToList()[0];

                return new ResponseModel { IsError = false, ErrorMessage = result.ErrorMessage, ValidateResponse = result.ValidateResponse };

            }
            catch (Exception ex)
            {
                return new ResponseModel { IsError = true, ErrorId = 1, ErrorMessage = (ex.InnerException != null ? ex.InnerException.ToString() : ex.Message), ValidateResponse = (ex.InnerException != null ? ex.InnerException.ToString() : ex.Message) };
            }

        }

        [HttpPost]
        [Route("refresh")]
        public LoginResponseModel Refresh([FromBody] RefreshTokenRequestModel requesModel)
        {
            if (requesModel == null || requesModel.Token == null)
            {
                return new LoginResponseModel { IsAuthSuccessful = false, User = null, ErrorMessage = "Invalid request" };
            }
            string aTkn = requesModel.Token;
            var principal = _jwtUtility.GetPrincipalFromExpiredToken(aTkn);
            var exp = principal.Claims.First(x => x.Type == "exp").Value;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(exp));
            var tokenExpiryTime = dateTimeOffset.LocalDateTime;

            string rTkn = requesModel.RefreshToken;
            var rfrshPrincipal = _jwtUtility.GetPrincipalFromExpiredToken(rTkn);
            var rfrshExp = rfrshPrincipal.Claims.First(x => x.Type == "exp").Value;
            //var tokenExpiryTime = Convert.ToDouble(rfrshExp).UnixTimeStampToDateTime();
            DateTimeOffset refDateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(rfrshExp));
            var refTokenExpiryTime = refDateTimeOffset.LocalDateTime;
            if (refTokenExpiryTime < DateTime.Now)
            {
                return new LoginResponseModel { IsAuthSuccessful = false, User = null, ErrorMessage = "Refresh Token Expired. Relogin" };
            }

            var username = rfrshPrincipal.Identity.Name; //this is mapped to the Name claim by default
            var user = new UserDetailsModel
            {
                IsError = true,
                ErrorMessage = "",
                ValidateResponse = "",
                UserName = username
            };
            List<Role> roles = this.GetRolesByUserName(username);

            if (roles != null && roles.Count > 0)
            {
                user.RoleNames = string.Join(",", roles.Select(x => x.RoleName)); ;//store rolenames as csv value here
                TokenModel token = _jwtUtility.GetTokenObject(HttpContext, user);
                if (token == null)
                {
                    var msg = "Get Access Token Failed. Relogin.";
                    user.ErrorMessage = msg;
                    user.ValidateResponse = msg;
                    return new LoginResponseModel { IsAuthSuccessful = false, User = user, ErrorMessage = msg };
                }
                user.IsError = false;
                user.Token = token.CurrentToken;
                user.RefreshToken = token.RotativeToken;
                return new LoginResponseModel { IsAuthSuccessful = true, User = user, ErrorMessage = "" };
            }
            else
            {
                var msg = "Get Access Token Failed. Relogin.";
                user.ErrorMessage = msg;
                user.ValidateResponse = msg;
                return new LoginResponseModel { IsAuthSuccessful = false, User = user, ErrorMessage = msg };
            }
        }
        private List<Role> GetRolesByUserName(string userName)
        {
            //try
            //{
            List<Role> roles = _context.RoleModels.FromSqlRaw(Constants.ua_SP_GetRoleByUserName, userName).ToList();

            return roles;
            //}
            //catch (Exception ex)
            //{
            //    return roles;
            //}
        }

        [HttpPost]
        [Route("check-user-access")]
        public UserAccessCheckResponseModel CheckUserAccessibility([FromBody] UserAccessCheckRequestModel request)
        {
            if (!string.IsNullOrEmpty(request?.PathURL) && request.PathURL.StartsWith("/"))
            {
                request.PathURL = request.PathURL.Substring(1);
            }

            if (string.IsNullOrWhiteSpace(request?.AccessLevel))
            {
                request.AccessLevel = null;
            }
            var userResponse = _context.UserAccessCheckResponseModel.FromSqlRaw(Constants.ua_sp_CheckUserAccess,
                request.UserGUID, request.PathURL, request.AccessLevel).ToList()[0];
            return userResponse;
        }

    }
}
