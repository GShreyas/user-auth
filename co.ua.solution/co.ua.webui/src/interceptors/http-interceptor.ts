import axios from 'axios';
import { getConfig } from '../services';

const useAxiosInterceptor = () => {
  console.error('getConfig()?.apiServerUrl',getConfig()?.apiServerUrl)
  const axiosApiInstance = axios.create({
    baseURL: getConfig()?.apiServerUrl,
  });

  axiosApiInstance.interceptors.request.use(
    (config) => {
      console.log('interceptor-request', config);
      // const token = sessionStorage.getItem('access_token');
      // if (token) {
      //   config.headers.Authorization = `Bearer ${token}`;
      // }
      return config;
    },
    (error) => {
      // Handle request errors here
      return Promise.reject(error);
    }
  );

  // Response interceptor
  axiosApiInstance.interceptors.response.use(
    (response) => {
      console.log('interceptor-response', response);
      // You can modify the response data or perform some post-processing here
      return response;
    },
    (error) => {
      // Handle response errors here, such as authentication failures or network errors
      return Promise.reject(error);
    });

  return { axiosApiInstance };
};

export default useAxiosInterceptor;
