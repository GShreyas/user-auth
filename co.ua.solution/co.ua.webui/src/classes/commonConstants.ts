export default class CommonConstants {
    public static readonly appConfigFilePath: string = '/config.json';
    public static readonly ipifyUrl: string = 'https://api.ipify.org/?format=json';
    public static readonly ReadAccess: string = 'Read'
    public static readonly PermissionObjectKey: string = 'PermissionObjectKey'
}