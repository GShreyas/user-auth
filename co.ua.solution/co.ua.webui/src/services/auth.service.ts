import { ILoginRequest } from '../interfaces/auth.interface';

// import { ToastrService } from "./prime-toastr.service";
import useAuth from "../hooks/auth";
import useAxiosInterceptor from "../interceptors/http-interceptor";
import AESHelper from './aesHelper.service';

const UserSignIn = async (data: ILoginRequest) => {
  try {
    const { axiosApiInstance } = useAxiosInterceptor();
    const { setUser } = useAuth();

    const response = await axiosApiInstance.post(`auth/validate-user`, data);
    const _response = response;
    console.error(_response);
    if (_response.data.isAuthSuccessful && _response.data.user) {

      const encrData = AESHelper.set(_response.data.user);
      
      setUser(encrData);
      return _response;
    } else {
    // //   ToastrService.error(_response.message, "Error");
      throw new Error(_response.data.errorMessage);
    }
  } catch (err) {
    // ToastrService.error(err.message, "Error");
    throw new Error();
  }
};

export { UserSignIn };
