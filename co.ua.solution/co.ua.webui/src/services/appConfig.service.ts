import CommonConstants from "../classes/commonConstants";
import { IAppConfig } from "../interfaces/appConfig.interface";

class AppConfig {
  private config: IAppConfig | null = null;
  private static instance: AppConfig | null = null;

  constructor() {
    this.config = null;
  }

  public static getInstance() {
    if (!AppConfig.instance) {
      AppConfig.instance = new AppConfig();
    }
    return AppConfig.instance;
  }

  async loadConfig() {
    try {
      const response = await fetch(CommonConstants.appConfigFilePath);
      if (response.ok) {
        this.config = await response.json();
      } else {
        throw new Error('Failed to load config');
      }
    } catch (error) {
      console.error('Error loading config:', error);
    }
  }

  get apiServerUrl() {
    console.log(this.config?.apiServerUrl)
    return this.config?.apiServerUrl || '';
  }
}

export default AppConfig.getInstance();