import CommonConstants from "../classes/commonConstants"
import useAxiosInterceptor from '../interceptors/http-interceptor'

export const getIp = async (): Promise<string> => {
    
    try{
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const { axiosApiInstance } = useAxiosInterceptor();
        const ip = (await axiosApiInstance.get(CommonConstants.ipifyUrl)).data.ip;
        console.log(ip);
        return ip;
    }
    catch(error){
        console.log(error);
        return ''
    }
} 