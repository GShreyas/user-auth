import CryptoJS from 'crypto-js';

class AESHelper {
  private iv: string = '@qwertyuiop12344';
  private key: string = "123456$#@$^@1ERF";

  aesKey(): string {
    this.key = "123456$#@$^@1ERF";
    return this.key;
  }

  encrypt(valueToEncrypt: string): string {
    const key = CryptoJS.enc.Utf8.parse(this.key);
    const iv = CryptoJS.enc.Utf8.parse(this.iv);

    const encryptedValue = CryptoJS.AES.encrypt(valueToEncrypt, key, {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Iso10126,
    });

    console.error('encryptedValue.toString()',encryptedValue.toString())

    return encryptedValue.toString();
  }

  decrypt(encryptedValue: string): string {
    const key = CryptoJS.enc.Utf8.parse(this.key);
    const iv = CryptoJS.enc.Utf8.parse(this.iv);

    const decrypted = CryptoJS.AES.decrypt(encryptedValue, key, {
      keySize: 128 / 8,
      iv: iv,
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Iso10126,
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  set(value: object){
   try{

    const secretKey = this.key;
    const iv = CryptoJS.enc.Utf8.parse(this.key);
    
    const encryptedData = CryptoJS.AES.encrypt(JSON.stringify(value), secretKey, {
      iv,
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.Pkcs7,
    });
    
    const encryptedString = encryptedData.toString();
    
    return encryptedString;
   }
   catch(error){
    console.error(error)
   }
   

  }

  get(value){

    const secretKey = this.key; 
    const iv = CryptoJS.enc.Utf8.parse(this.key); 

    const bytes = CryptoJS.AES.decrypt(value, secretKey, {
      iv,
      mode: CryptoJS.mode.CFB,
      padding: CryptoJS.pad.Pkcs7,
    });
    
    const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    
    return decryptedData
  }

  randomKey(length: number): string {
    let result = '';
    const characters = '@abcdefghijklmnopqrstuvwxyz123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}

export default new AESHelper();
