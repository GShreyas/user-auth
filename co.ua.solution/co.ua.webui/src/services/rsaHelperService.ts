import Forge from 'node-forge';

class RSAHelper {
  publicKey: string = `-----BEGIN PUBLIC KEY-----
  MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgGbhc2GRAoFe/CLj+ds3URzWPARD
  Koc35tOuWZjgYdN2sWtFBHosF3JgdSxYO1BCTCJeJGcZeddnO8DUL9AyrJ1q1OZ+
  lP67cFyKvJRSQEgUWSusiCAsR0qT1xX51OIlaRdDp0MyH+YH82m5000W55OwkWjH
  KD2wRMR52NMOUZBvAgMBAAE=
  -----END PUBLIC KEY-----`;

  encryptWithPublicKey(valueToEncrypt: string): string {
    const rsa = Forge.pki.publicKeyFromPem(this.publicKey);
    return window.btoa(rsa.encrypt(valueToEncrypt.toString()));
  }
}

export default new RSAHelper();
