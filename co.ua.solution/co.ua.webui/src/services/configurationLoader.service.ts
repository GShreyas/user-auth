import axios from "axios";
import CommonConstants from "../classes/commonConstants";
import { IAppConfig } from "../interfaces/appConfig.interface";

let configData: IAppConfig | null= null;

export const loadConfig = async () => {
  try {
    const response = await axios.get(CommonConstants.appConfigFilePath);
    if (response) {
        console.log(response);
        configData = response.data;
    } else {
      throw new Error('Failed to load config');
    }
  } catch (error) {
    console.error('Error loading configuration:', error);
  }
};

export const getConfig = () => configData;
