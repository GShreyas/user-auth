/* eslint-disable prefer-const */
/* eslint-disable react-hooks/rules-of-hooks */
import CommonConstants from "../classes/commonConstants";
import useAxiosInterceptor from "../interceptors/http-interceptor";
import { getConfig } from "../services";
import AESHelper from '../services/aesHelper.service';


const useAuth = () => {

  const userKey: string =  getConfig()?.userKey as string;

  const setUser = (data: unknown) => {
    try{
      if (data) {
        sessionStorage.setItem(userKey, JSON.stringify(data));
        return true;
      }
    }
    catch(error){
      console.error(error)
    }
    
  };

  const getUser = () => {
    if(sessionStorage.getItem(userKey))
    {
      const currentUser = AESHelper.get(JSON.parse(sessionStorage.getItem(userKey) as string));
      return currentUser;
    } else {
      const tmp: unknown = {};
      return tmp;
    }
  };

  const deleteUser = () => {
    sessionStorage.removeItem(userKey);
  };

  const clearLocalStorage = () => {
    sessionStorage.clear();
  };

  const checkUserLoginStatus = () => {

    try {
      const token = getToken();

      if(!token) return false;
  
      const isTokenExpired = checkTokenExpiry(token);
  
      if (!isTokenExpired) {
        return true;
      } else {
        const isRefreshSuccess = tryRefreshingToken();
        if(!isRefreshSuccess) {
          return false;
        } else {
          checkUserLoginStatus();
        }
      }

    }
    catch(error) {
      return false;
    }
   

  }

  const getToken = () => {
    const decrData = AESHelper.get(JSON.parse(sessionStorage.getItem(userKey) as string));
    const token = decrData.token;
    return token;
  }



  const checkTokenExpiry = (token: string) => {

    try {
      // Extract the payload from the token
      const payloadBase64 = token.split('.')[1];
      const payload = JSON.parse(atob(payloadBase64));
  
      // Check if the expiration time is in the past
      return payload.exp < Math.floor(Date.now() / 1000);
    } catch (error) {
      // Handle decoding errors
      console.error('Error decoding JWT:', error);
      return true; // Treat decoding errors as expired
    }
    

  }

  const tryRefreshingToken = async () => {
    
    let isRefreshSuccess: boolean = false;

    const currentUser = getUser();

    if(!currentUser || !currentUser.token || !currentUser.refreshToken){
      return isRefreshSuccess;    
    }

    const token: string = currentUser.token;
    const refreshToken: string = currentUser.refreshToken;
    const requestModel = { token: token, refreshToken: refreshToken };

    const { axiosApiInstance } = useAxiosInterceptor();

    const response = await axiosApiInstance.post(`auth/refresh`, requestModel);

    let user = getUser();

    if(user && !response.body.user?.isError){
        user.token = response.body.user.token;
        user.refreshToken = response.body.user.refreshToken;

        const encrData = AESHelper.set(user.data.user);

        setUser(encrData);
        isRefreshSuccess = true;
    }

    return isRefreshSuccess;


  }

  const checkRouteAccess = (route: string) => {
    const currentUser = getUser();
    let hasAccess: boolean = false;
    console.error('currentUser',currentUser)
    if (currentUser && currentUser.userId) {
      console.log('<:::Thru Route:::>' + route);
      let checkAccessResult = checkAccess(currentUser.userId, route);
      console.error('checkAccessResult',checkAccessResult)
    }
    return hasAccess
  }

  const checkAccess = async (userId: string, route: string) => {
    let checkResult = false;

    let requestModel = {
      userGuid : userId,
      pathUrl : route
    };

    const { axiosApiInstance } = useAxiosInterceptor();

    const response = await axiosApiInstance.post(`auth/check-user-access`, requestModel);

    console.error('response',response);
    const checkAccessResult = response.data;
    let ual: string = ''

    if(!checkAccessResult || checkAccessResult.isError){
      console.log('Network error: ' + checkAccessResult.errorMessage);
    } else  if ( checkAccessResult &&  checkAccessResult.totalAccessCount > 0 ) {
        ual = (checkAccessResult.accessLevels || '').split(',');
        checkResult = ual.includes(CommonConstants.ReadAccess);
    }

    if(checkResult) {

      setRouteAccessObject(route, checkResult, ual);

    } else {
      console.log('Blocked Path is: '+ route);
    }

    return checkResult;

  }

  const setRouteAccessObject = (route: string, checkResult: boolean, ual: string) => {

    let currentUser = null;
    let permissionObject = null

    if(sessionStorage.getItem(userKey)){
      currentUser = getUser();
    } else return 

    let permItemKey = {id: currentUser.userId, path: route};

    if(sessionStorage.getItem(CommonConstants.PermissionObjectKey)){
      permissionObject = JSON.parse(sessionStorage.getItem(CommonConstants.PermissionObjectKey) as string);
    }
    if(!permissionObject){
        permissionObject = new Map();
    }
    if(checkResult){

        const iKey = JSON.stringify(permItemKey);
        permissionObject[iKey] = ual;
        sessionStorage.setItem(CommonConstants.PermissionObjectKey, JSON.stringify(permissionObject));
    }
    else {

        const iKey = JSON.stringify(permItemKey);
        delete permissionObject[iKey];

    }

  }

  return {
    setUser,
    getUser,
    deleteUser,
    clearLocalStorage,
    checkUserLoginStatus,
    checkRouteAccess
  };
};

export default useAuth;
