// import { Route, Routes } from 'react-router-dom'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import './App.css'
// import Sidebar from './components/Sidebar'
// import React from 'react'
import Login from './pages/Login/Login'
import Main from './pages/Main/Main'
import React from 'react'
import AuthenticationGuard from './guards/authentication.guard'
import RouteGuard from './guards/route.guard'
const Attributes = React.lazy(() => import('./pages/Main/Attributes/Attributes'))
const NotFound = React.lazy(() => import('./pages/NotFound/NotFound'))

const App = () => {
  
  return (
    <>
      {/* <div className="main-container"> */}

      <BrowserRouter>
        <Routes>
          <Route path="login" element={<Login />} />
          
          <Route element={<AuthenticationGuard />}>

            <Route element={<RouteGuard />}>

              <Route path="main" element={<Main />}>

                <Route
                  path="attributes"
                  element={
                    <React.Suspense fallback={<>...</>}>
                      <Attributes />
                    </React.Suspense>
                  }
                />
                </Route>

            </Route>
            
          </Route>
          <Route index element={<Navigate to="login" />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
         
        {/* <div className="sidebar">
          <Sidebar />
        </div> */}

          {/* <Routes>
            <Route
              index
              element={
                <React.Suspense fallback={<>...</>}>
                  <Attributes />
                </React.Suspense>
              }
            />


            <Route
              element={
                <React.Suspense fallback={<>...</>}>
                  <Attributes />
                </React.Suspense>
              }
            />
          </Routes> */}
      {/* </div> */}
    </>
  )
}

export default App
