export interface IUser {
    userId: string;
    userName: string;
    firstName: string;
    initials: null | string;
    lastName: string;
    jobTitle: null | string;
    userEmail: string;
    isUserLocked: boolean;
    isActive: boolean;
    inactiveDate: null | string;
    inactiveReason: null | string;
    roleIds: null | string;
    roleNames: string;
    groupIds: null | string;
    groupNames: null | string;
    refreshToken: string;
    createdBy: string;
    location: string;
    mugShot: null | string;
    issa: boolean;
    tokenID: string;
    token: string;
    errorId: number;
    isError: boolean;
    errorMessage: string;
    validateResponse: string;
  }
  