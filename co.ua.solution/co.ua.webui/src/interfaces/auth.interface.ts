import { IRequest } from "./requestResponse.interface";


interface IUser {
    userId?: string;
    userName?: string;
    firstName?: string;
    initials?: string;
    lastName?: string;
    jobTitle?: string;
    userEmail?: string;
    isUserLocked?: boolean;
    isActive?: boolean;
    inactiveDate?: Date;
    inactiveReason?: string;
    roleIds?: string;
    roleNames?: string;
    groupIds?: string;
    groupNames?: string;
    refreshToken?: string;
    createdBy?: string;
    location?: string;
    mugShot?: Uint8Array;
    isSA?: boolean;
    tokenID?: string;
    token?: string;
    errorId: number;
    isError?: boolean;
    errorMessage?: string;
    validateResponse?: string;
}
  

export interface ILoginResponse {
    isAuthSuccessful : boolean,
    errorMessage: string,
    userDetailsModel: IUser
}

export interface IUserLoginData extends IRequest {
    username: string,
    password: string,
}

export interface ILoginRequest{
    data: string,
    aesKey: string
}

export interface IAuthService {
    login(data: ILoginRequest): Promise<ILoginResponse>
}