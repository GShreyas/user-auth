export interface IAppConfig {
  name: string,
  apiServerUrl: string,
  userKey: string
}