import { Navigate, Outlet } from "react-router-dom";
import useAuth from "../hooks/auth";

const AuthenticationGuard: React.FC = () => {
  const { checkUserLoginStatus }  = useAuth();
  const isUserLoggedIn = checkUserLoginStatus();

  return isUserLoggedIn ? (
    <Outlet />
  ) : (
    <Navigate to="/login" replace />
  );
};

export default AuthenticationGuard;
