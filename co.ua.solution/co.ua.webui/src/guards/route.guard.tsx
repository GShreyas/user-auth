import { Navigate, Outlet, useLocation } from "react-router-dom";
import useAuth from "../hooks/auth";

const RouteGuard: React.FC = () => {
  const { checkRouteAccess } = useAuth();
    const location = useLocation();
    console.error('locationlocationlocation',location)
  const hasAccess = checkRouteAccess(location.pathname)

  return hasAccess ? (
    <Outlet />
  ) : (
    <Navigate to="/login" replace />
  );
};

export default RouteGuard;
