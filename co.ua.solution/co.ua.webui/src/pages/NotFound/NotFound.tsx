import React from 'react'

const NotFound = () => {
    return (
        <div className='notFound-container'>
            <div className='notFound-box'>
                <h3 >404 not found!</h3>
                <p>Stop lurking around!</p>
            </div>
        </div>
    )
}

export default NotFound