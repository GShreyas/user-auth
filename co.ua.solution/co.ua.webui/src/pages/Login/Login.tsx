import { useState } from "react";
import './Login.scss'
import { UserSignIn } from "../../services/auth.service";
import { ILoginRequest } from "../../interfaces/auth.interface";
import AESHelper from "../../services/aesHelper.service";
import RSAHelper from "../../services/rsaHelperService";
import { getIp } from "../../services";
import { useNavigate } from "react-router";

const Login = () => {
    const navigate = useNavigate();
    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');

    const setChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name,value} = e.target
        if(name=="username") setUsername(value);
        if(name=="password") setPassword(value);
    }

    const handleSubmit = async (e: { preventDefault: () => void; }) => {
        e.preventDefault();

        const userData = {
            username: username,
            password: password,
            clientIP: await getIp()
        }

        const aesKeyValue = AESHelper.aesKey();
        const rsaKey = RSAHelper.encryptWithPublicKey(aesKeyValue);
        const encJsonUser = AESHelper.encrypt(JSON.stringify(userData));

        const loginPayload: ILoginRequest = { data: encJsonUser, aesKey: rsaKey };

        await UserSignIn(loginPayload)
        .then((res) => {
            console.log(res)
            navigate("/main/attributes");
            // setLoading(false);
        })
        .catch(() => {
            // setLoading(false);
        });
    }



    return (
        <>
            <div className="main">
                <div className="login-container">
                    <div className="form-container">
                        <form onSubmit={handleSubmit}>

                            <div className="wrap-input" >
                                <input type="text" name="username" value={username} onChange={setChange} placeholder="Enter Username" maxLength={25} required />
                                <span className="focus-input"></span>
                                {/* <span className="symbol-input">
                                    <i className="material-icons">
                                        person
                                    </i>
                                </span> */}
                            </div>

                            <div className="wrap-input" >
                                <input type="password" name="password" value={password} onChange={setChange} placeholder="Enter Password" required/>
                                <span className="focus-input"></span>
                                {/* <span className="symbol-input">
                                    <i className="material-icons">
                                        lock
                                    </i>
                                </span> */}
                            </div>

                            <div className="login-btn-container">
                                <button type="submit" className='login-btn'>Log In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </>
    )

}

export default Login