// AuthContext.tsx
import React, { ReactNode, createContext, useContext } from 'react';
import axios from 'axios';

// Define the authentication data interface
interface AuthData {
  login: (username: string, userpassword: string, userIP: string) => Promise<User | null>;
}

interface User {
  username: string;
  token: string;
}

const AuthContext = createContext<AuthData | undefined>(undefined);

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('useAuth must be used within an AuthContextProvider');
  }
  return context;
};

export const AuthProvider = ({ children }: {children: ReactNode }) => {

  const login = async (username: string, userpassword: string, userIP: string): Promise<User | null> => {
    try {
      const response = await axios.post('', { username, userpassword, userIP });
      console.error(response);
      const authResponse = response.data;
      if (authResponse.isAuthSuccessful && authResponse.user && authResponse.user.isValidUser) {
        const user = authResponse.user;
        sessionStorage.setItem('userToken', user.token);
        return user;
      } else {
        sessionStorage.removeItem('userToken');
        return null;
      }
    } catch (error) {
      console.error('Error during login:', error);
      return null;
    }
  };

  const authFunctions = {
    login,
  };

  return (
    <AuthContext.Provider value={authFunctions}>
      {children}
    </AuthContext.Provider>
  );
};
