import React, { ReactNode, createContext, useContext } from 'react';
import axios from 'axios';

const DataContext = createContext({});

interface IPostData {
    clientIP?: string;
    userGUID?: string;
    userName?: string;
  }

export const useData = () => {
  return useContext(DataContext);
};

export const DataProvider = ({ children }: { children: ReactNode }) => {
  const apiServerUrl = 'https://your-api-server-url.com';

  const sendPostRequest = async (url: string, postData: IPostData) => {
    if (!postData) {
      postData = {};
    }

    postData.clientIP = sessionStorage.getItem('clientIP') || '';
    const user = getCurrentUser();
    postData.userName = `${user.firstName} ${user.lastName}`;
    postData.userGUID = user.userId;

    const response = await axios.post(`${apiServerUrl}${url}`, postData);

    return response.data;
  };

  const sendGetRequest = async (url: string) => {
    const response = await axios.get(`${apiServerUrl}${url}`);
    return response.data;
  };

  const getCurrentUser = () => {
    // Implement your user retrieval logic here
    return {
      userId: '12345',
      firstName: 'John',
      lastName: 'Doe',
    };
  };

  return (
    <DataContext.Provider value={{ sendPostRequest, sendGetRequest }}>
      {children}
    </DataContext.Provider>
  );
};
